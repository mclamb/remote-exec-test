#!/bin/bash
set -euxo pipefail

sed -i 's@^PasswordAuthentication no$@PasswordAuthentication yes@' /etc/ssh/sshd_config
sed -i 's@^#PermitRootLogin prohibit-password@PermitRootLogin yes@' /etc/ssh/sshd_config
service ssh restart

count=0
while [[ $${count} -lt 6 ]]; do
	echo "Checking if node is ready..."
	sleep 5
	count=$((count+1))
done

echo "IT'S READY!"

touch /tmp/finished-user-data
