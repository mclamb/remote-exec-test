terraform {
	required_version = ">= 0.12.2"
}

provider "packet" {
	version    = ">= 2.2.1"
	auth_token = "${var.packet_auth_token}"
}

data "template_file" "controller" {
	template = "${file("node_config.tpl")}"
}

resource "packet_device" "k8s_controller_workers" {
	count            = "${var.num_controller_worker_nodes}"
	hostname         = "${format("${var.k8s_cluster_name}-controller-%02d",count.index)}"
	operating_system = "ubuntu_18_04"
	plan             = "${var.packet_plan_controller}"
	facilities       = ["${var.packet_facility}"]
	user_data        = "${data.template_file.controller.rendered}"
	tags             = ["kubernetes", "controller-${var.k8s_cluster_name}"]
	billing_cycle    = "hourly"
	project_id       = "${var.packet_project_id}"
	network_type     = "layer3"


	provisioner "remote-exec" {
		inline = [
			#"cloud-init status --wait"

			# Clever trick to tail cloud-init-output.log and bail out (sed 'q') only
			# when it comes to the last line in the cloud init script (which is touch
			# /tmp/finished-user-data) -- Because set -euxo nopipefail is set at the
			# top of the script, it will output the command having gotten there only
			# if nothing else failed
			"/bin/bash -c \"timeout 1200 sed '/finished-user-data/q' <(tail -f /var/log/cloud-init-output.log)\""
		]

		connection {
			type = "ssh"
			host = "${self.network.0.address}"
			user = "root"
			password = "${self.root_password}"
		}
	}

	lifecycle {
		#prevent_destroy = true
		ignore_changes = [ user_data ]
	}
}

output "controller_addresses" {
	description = "Kubernetes Controller IP Addresses"
	value       = "${packet_device.k8s_controller_workers.*.network.0.address}"
}
output "controller_root_password" {
	#sensitive   = true
	description = "Kubernetes Controller root password"
	value       = "${packet_device.k8s_controller_workers.*.root_password}"
}
