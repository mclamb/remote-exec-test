#!/bin/bash

K8S_CLUSTER_NAME="$(basename $(dirname $(pwd)))"

cat <<EOF > vars.tf

variable "infrastructure_provider" {
  default     = "${INFRASTRUCTURE_PROVIDER:-packet}"
  description = "Provider of underlying nodes"
}

variable "num_controller_worker_nodes" {
  default     = "1"
  description = "Number of controller/worker nodes"
}

variable "num_worker_nodes" {
  default     = "0"
  description = "Number of worker-only nodes"
}

variable "k8s_cluster_name" {
  description = "Name of your cluster. Alpha-numeric and hyphens only, please."
  default     = "${K8S_CLUSTER_NAME}"
}

variable "packet_plan_controller" {
  description = "Packet plan for K8s controller nodes"
  default     = "baremetal_0"
}

variable "packet_plan_worker" {
  description = "Packet plan for K8s worker nodes"
  default     = "baremetal_0"
}

variable "packet_auth_token" {
  default = "${PACKET_AUTH_TOKEN}"
}

variable "packet_facility" {
  description = "Packet Facility"
	default     = "${PACKET_FACILITY:-ewr1}"
}

variable "packet_project_id" {
	default = "${PACKET_PROJECT_ID}"
}
EOF
